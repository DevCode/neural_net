typedef struct {
  int input_layer_size; 
  int output_layer_size; 
  int hidden_layer_depth; 
  int *hidden_layer_size;
  float **weights; 
  float **values;
} neural_network;

typedef struct {
  int number_of_samples; 
  int width_of_samples; 
  int height_of_samples; 
  unsigned char *data; 
  unsigned char *answers;
} test_samples;

/* general neural network stuff */
neural_network *init_neural_network(int input_layer_size, int hidden_layer_depth, int *hidden_layer_size, int output_layer_size);
float *emulate(neural_network *nn, float *input);
float adjust_weights(neural_network *nn, float *input, float *expected_output, float learning_rate);
void teach(neural_network *nn, int number_of_samples, float *inputs, float *expected_outputs, float learning_rate, int passes);

/* io - related stuff */

int read_int(FILE *file); 
int read_int_big_endian(FILE *file);
void write_int(int i, FILE *file);
void write_neural_network(neural_network *nn, char *str);
neural_network *read_neural_network(char *str);
test_samples *read_test_samples(char *name_of_samples, char *name_of_answers);

/* inline methods */
static inline float sigmoid(float x);
static inline float random_weight(float min, float max);
