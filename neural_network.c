#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "neural_network.h"

static inline float sigmoid(float x) { return  0.5 * x / (1 + (x > 0 ? x : -x)) + 0.5; }
static inline float random_weight(float min, float max) { return (float) ((rand() / (float)RAND_MAX) * (max - min)) + min; }

neural_network *init_neural_network(int input_layer_size, int hidden_layer_depth, int *hidden_layer_size, int output_layer_size)
{
  srand(time(NULL)); 
  neural_network *nn = malloc(sizeof(neural_network));
  nn->input_layer_size = input_layer_size; 
  nn->output_layer_size = output_layer_size;
  nn->hidden_layer_depth = hidden_layer_depth; 
  nn->hidden_layer_size = malloc(sizeof(int) * hidden_layer_depth);
  nn->weights  = malloc(sizeof(float *) * (hidden_layer_depth+1));
  nn->values = malloc(sizeof(float *) * (hidden_layer_depth+1));
  for (int i = 0; i < hidden_layer_depth; i++)
    nn->hidden_layer_size[i] = hidden_layer_size[i];
  
  for (int i = 0; i < hidden_layer_depth+1; i++)
  {
    int len_prev = (i != 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
    int len = (i != nn->hidden_layer_depth) ? hidden_layer_size[i] : output_layer_size;
    nn->weights[i] = malloc(sizeof(float) * len * len_prev);
    nn->values[i] = malloc(sizeof(float) * len * len_prev);
  }
  for (int i = 0; i < hidden_layer_depth+1; i++)
  {
    int len_prev = (i > 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
    int len = (i < nn->hidden_layer_depth) ? nn->hidden_layer_size[i] : nn->output_layer_size;
    int size = len * len_prev;
    for (int j = 0; j < size; j++)
    {
      nn->weights[i][j] = random_weight(-0.5, 0.5);
    }
  }
  return nn;
}

float *emulate(neural_network *nn, float *input)
{
  float *output = malloc(sizeof(float) * nn->output_layer_size); 
  int stride = sizeof(float) * nn->input_layer_size;
  void *ptr = (nn->values[0]);
  for (int i = nn->hidden_layer_size[0]-1; i--; )
  {
    memcpy(ptr, (void *) input, stride);
    ptr+=stride;
  }
  
  for (int i = 0; i <= nn->hidden_layer_depth; i++)
  {
    int len = (i < nn->hidden_layer_depth) ? nn->hidden_layer_size[i] : nn->output_layer_size;
    int len_prev = (i != 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
    int len_next = ((i+1) < nn->hidden_layer_depth) ? nn->hidden_layer_size[i+1] : nn->output_layer_size;
    int off = 0;
    for (int j = 0; j < len; j++)
    {
      register float sum = 0;
      int count = len_prev+off;
      float *end = (nn->values[i+1]+j+(len*len_next));
      for (int k = off; k < count; k++) // critical
        sum = sum + (*(nn->values[i]+k)) * (*(nn->weights[i]+k));
      
      float activation = sigmoid(sum);
      if (i < nn->hidden_layer_depth)
      {
        for (float *ptr = (nn->values[i+1]+j); ptr < end; ptr += len) // not critical
          (*ptr) = activation;
      }
      else if (i == nn->hidden_layer_depth)
          output[j] = activation; 
          
      off += len_prev;
    }
  }
  return output;
}

float adjust_weights(neural_network *nn, float *input, float *expected_output, float learning_rate)
{
  float error_ges = 0;
  float **error = malloc(sizeof(float *) * (nn->hidden_layer_depth+1));
  float *output = emulate(nn, input);
  const int hd = nn->hidden_layer_depth;
  
  for (int i = 0; i < hd; i++)
    error[i] = malloc(sizeof(float *) * nn->hidden_layer_size[i]);
  
  error[hd] = malloc(sizeof(float *) * nn->output_layer_size);
   
  for (int i = 0; i < nn->output_layer_size; i++) 
  {
    error[hd][i] = output[i] * (1 - output[i]) * (expected_output[i] - output[i]);
    error_ges += fabs(error[hd][i]);
    float err = learning_rate * error[hd][i];
    int off = i * nn->hidden_layer_size[hd-1];
    int end = nn->hidden_layer_size[hd-1] + off;
    for (int j = off; j < end; j++)
      *(nn->weights[hd]+j) = *(nn->weights[hd]+j) + err * *(nn->values[hd]+j); 
  }
  
  for (int i = hd-1; i >= 0; i--)
  {
    for (int j = 0; j < nn->hidden_layer_size[i]; j++)
    {
      float err_backprop = 0;
      int count = (i+1) != hd ? nn->hidden_layer_size[i+1] : nn->output_layer_size;
      int len_prev = (i != 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
      int off = (j * len_prev);
      int len = off+len_prev;
      
      for (int k = count-1; k--; )
        err_backprop = err_backprop + *(error[i+1]+k) * *(nn->weights[i+1]+j+(k * nn->hidden_layer_size[i]));
      
      // use activity not one input :^)
      error[i][j] = nn->values[i+1][j] * (1 - nn->values[i+1][j]) * err_backprop;
      
      float err = learning_rate * error[i][j];
      for (int k = off; k < len; k++)
        *(nn->weights[i]+k) = *(nn->weights[i]+k) + err * *(nn->values[i]+k);
    }
  }
  
  free(output); 
  
  for (int i = 0; i < hd+1; i++)
    free(*(error+i));
  free(error);

  return error_ges;
}

void teach(neural_network *nn, int number_of_samples, float *inputs, float *expected_outputs, float learning_rate, int passes)
{
  int number_of_inputs = nn->input_layer_size;
  int number_of_outputs = nn->output_layer_size;
  int print_after = number_of_samples / 10000;
  float percent = 0;
  
  for (int i = 0; i < passes; i++)
  {
    for (int j = 0; j < number_of_samples; j++)
    {
      float err = adjust_weights(nn, &inputs[j*number_of_inputs], &expected_outputs[j*number_of_outputs], learning_rate);
      if (print_after-- == 0)
      {
        print_after = number_of_samples / 10000;
        percent = j / (float)number_of_samples;
        printf("Pass %d of %d [ %.2f % ] \r", i+1, passes, (percent * 100));
      }
    }
    percent = 0;
    printf("Pass %d of %d [ %.2f % ] %d elements checked of %d elements overall \n", i+1, passes, 100.0, number_of_samples, number_of_samples);
  }
}

int read_int(FILE *file) { return (int) (fgetc(file) << 0 | fgetc(file) << 8 | fgetc(file) << 16 | fgetc(file) << 24); }

// stupid fukks
int read_int_big_endian(FILE *file) { return (int) (fgetc(file) << 24 | fgetc(file) << 16 | fgetc(file) << 8 | fgetc(file) << 0); }

void write_int(int i, FILE *file) { fwrite(&i, sizeof(int), 1, file); }

test_samples *read_test_samples(char *name_of_samples, char *name_of_answers)
{
  FILE *samples = fopen(name_of_samples, "rb");
  FILE *answers = fopen(name_of_answers, "rb");
  test_samples *ts = malloc(sizeof(test_samples));
  read_int_big_endian(samples); // skip magic numbr
  read_int_big_endian(answers); read_int_big_endian(answers);
  ts->number_of_samples = read_int_big_endian(samples);
  ts->width_of_samples = read_int_big_endian(samples); 
  ts->height_of_samples = read_int_big_endian(samples);
  ts->data = malloc(sizeof(char) * ts->number_of_samples * ts->width_of_samples * ts->height_of_samples);
  ts->answers = malloc(sizeof(char) * ts->number_of_samples);
  
  fread(ts->data, sizeof(char), ts->number_of_samples * ts->width_of_samples * ts->height_of_samples, samples);
  fread(ts->answers, sizeof(char), ts->number_of_samples, answers);
  fclose(samples);
  fclose(answers);
  return ts;
}

void write_neural_network(neural_network *nn, char *str)
{
  FILE *file = fopen(str, "wb");
  if (file == NULL)
  {
    printf("Error opening file %s \n", str);
    return;
  }
  write_int(nn->input_layer_size, file); 
  write_int(nn->hidden_layer_depth, file); 
  for (int i = 0; i < nn->hidden_layer_depth; i++)
    write_int(nn->hidden_layer_size[i], file); 
    
  write_int(nn->output_layer_size, file);
  
  for (int i = 0; i < nn->hidden_layer_depth+1; i++)
  {
    int len_prev = (i != 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
    int len = (i != nn->hidden_layer_depth) ? nn->hidden_layer_size[i] : nn->output_layer_size;
    fwrite(nn->weights[i], sizeof(float), len_prev * len, file);
  }
  fclose(file);
}

neural_network *read_neural_network(char *str)
{
  FILE *file = fopen(str, "rb");
  if (file == NULL)
  {
    printf("Error opening file %s \n", str);
    return NULL;
  }
  
  int input_layer_size = read_int(file);
  int hidden_layer_depth = read_int(file);
  int *hidden_layer_size = malloc(sizeof(int) * hidden_layer_depth); 
  
  for (int i = 0; i < hidden_layer_depth; i++)
    hidden_layer_size[i] = read_int(file); 
  
  int output_layer_size = read_int(file);
  
  neural_network *nn = init_neural_network(input_layer_size, hidden_layer_depth, hidden_layer_size, output_layer_size);
  
  printf("input_layer_size : %d \nhidden_layer_depth %d \n", input_layer_size, hidden_layer_depth);
  for (int i = 0; i < hidden_layer_depth; i++)
    printf(" hidden_layer_size[%d] : %d \n", i, hidden_layer_size[i]);
  
  printf("output_layer_size : %d \n", output_layer_size);
  
  for (int i = 0; i < nn->hidden_layer_depth+1; i++)
  {
    int len_prev = (i != 0) ? nn->hidden_layer_size[i-1] : nn->input_layer_size;
    int len = (i != nn->hidden_layer_depth) ? nn->hidden_layer_size[i] : nn->output_layer_size;
    fread(nn->weights[i], sizeof(float), len_prev * len, file);
  }
  
  fclose(file);
  return nn;
}