#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "neural_network.h"

#define REPEAT 500
#define DIGITS_TO_TRAIN 50000
#define DIGITS_TO_TEST 1000

int main(int argc, char *argv[])
{
  time_t start; 
  time_t end;
  
  time(&start);
  
  int hidden_size[1] = {125};
  
  test_samples *ts = read_test_samples("train-images", "train-labels");
  
  float *output = malloc(sizeof(float) * ts->number_of_samples * 10);
  float *input = malloc(sizeof(float) * ts->number_of_samples * ts->width_of_samples * ts->height_of_samples);
  
  printf("Number of items %d width[%d] height[%d] \n", ts->number_of_samples, ts->width_of_samples, ts->height_of_samples);
  
  neural_network *nn = init_neural_network(ts->width_of_samples * ts->height_of_samples, 1, hidden_size, 10);
  
  //neural_network *nn = read_neural_network("nn.bin");
  int len = ts->number_of_samples * ts->width_of_samples * ts->height_of_samples;
  for (int i = 0; i < len; i++)
  {
    if (ts->data[i] < 50)
      input[i] = 0; 
    else 
      input[i] = 1;
  }
  
  for (int i = 0; i < ts->number_of_samples; i++)
    output[0] = 0;
  
  for (int i = 0; i < ts->number_of_samples; i++)
  {
    int off = (i*10); 
    for (int j = 0; j < 10; j++)
    {
      if (j == ts->answers[i])
        output[j+off] = 1;
    }
  }
  
  // not needed anymore
  free(ts->answers);
  free(ts->data);
  
  
  printf("Repeat learning procedure %d times \n", REPEAT);
  
  teach(nn, DIGITS_TO_TRAIN, input, output, 0.125, REPEAT);
  
  time(&end);
  
  printf("It took %f seconds to train the neural network \n", difftime(end, start));
  
  float *out = emulate(nn, input);
  
  int error_count = 0;
  printf("Testing %d digits \n", DIGITS_TO_TEST);
  for (int i = DIGITS_TO_TRAIN; i < DIGITS_TO_TEST+DIGITS_TO_TRAIN; i++)
  {
    out = emulate(nn, input+(i*ts->width_of_samples * ts->height_of_samples));
    float answer = 0;
    int answer_index = 0;;
    int expected_answer = 0;
    for (int j = 0; j < 10; j++)
    {
      if (answer < out[j])
      {
        answer = out[j]; 
        answer_index = j;
      }
    }
    
    for (int j = 0; j < 10; j++)
    {
      if (output[i*10 + j] == 1.0)
        expected_answer = j;
    }
    if (output[i * 10 + answer_index] < 1.0)
      printf("error %d answer : %d expected answer : % d \n", error_count++, answer_index, expected_answer);
    free(out);
  }
  printf("%d errors found with %d digits checked \n", error_count, DIGITS_TO_TEST);
  
  write_neural_network(nn, "nn.bin");
  
  return 0;
}